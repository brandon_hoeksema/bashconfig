if [[ ! -f ~/.ssh/id_rsa.pub ]]; then
    ssh-keygen
    echo "Paste this into git provider"
    cat ~/.ssh/id_rsa.pub
fi

if [[ ! -d ~/history ]]; then
	mkdir ~/history
    echo "0" > ~/history/num_lines
    echo $(pwd) > ~/history/pwd
fi

if [[ -d ~/shconfig ]]; then
    ({
        cd ~/shconfig
        git pull > /dev/null 2>&1
        cd - > /dev/null 2>&1
    } &)
fi

if [[ -d ~/vimconfig ]]; then
    ({
        cd ~/vimconfig
        git pull > /dev/null 2>&1
        cd - > /dev/null 2>&1
    } &)
else
	git clone git@bitbucket.org:brandon_hoeksema/vimconfig.git
    if [[ ! -L ~/.vim ]]; then
        if [[ -d ~/.vim ]]; then
            mv ~/.vim ~/.vim.bak
        fi
        ln -s ~/vimconfig/.vim ~/.vim
        echo "source ~/vimconfig/.vimrc" >> ~/.vimrc
        echo "source ~/vimconfig/.ideavimrc" >> ~/.ideavimrc
    fi
fi



if [[ -d ~/bc_functions ]]; then
    ({
        cd ~/bc_functions
        git pull > /dev/null 2>&1
        cd - > /dev/null 2>&1
    } &)
else
    git clone git@bitbucket.org:brandon_hoeksema/bc_functions.git
fi

if [[ -d ~/executables ]]; then
    ({
        cd ~/executables
        git pull > /dev/null 2>&1
        cd - > /dev/null 2>&1
    } &)
else
	git clone git@bitbucket.org:brandon_hoeksema/executables.git
fi

export PATH=$PATH:$HOME/executables

case `uname` in
    Darwin)
        source ~/shconfig/mac_config.sh
    ;;
    Linux)
        source ~/shconfig/linux_config.sh
    ;;
esac

export PATH=$PATH:$HOME/bin
export PATH=$PATH:$HOME/.local/bin

alias sudo='sudo -E '
alias ranger='ranger --choosedir=$HOME/rangerdir; LASTDIR=`cat $HOME/rangerdir`; cd "$LASTDIR"'

function pcmd {
    HDIR="$HOME/history$(pwd)" && \
    mkdir -p $HDIR && \
    touch $HDIR/.history && \
    LASTPWD=$(tail -n 1 $HOME/history/pwd) && \
    LASTLINES=$(tail -n 1 $HOME/history/num_lines) && \
    BHLINES=$(wc -l $HISTFILE | awk '{print $1}') && \
    echo $BHLINES > $HOME/history/num_lines && \
    echo $(pwd) > $HOME/history/pwd && \
    LASTBHLINE=$(tail -n 1 $HISTFILE) && \
    LASTHLINE=$(tail -n 1 $HDIR/.history) && \
    if [ "$BHLINES" -gt "$LASTLINES" ] || ( [[ "$LASTPWD" == "$(pwd)" ]] && [[ "${LASTBHLINE}" != "${LASTHLINE}" ]] ); then echo "$LASTBHLINE" >> $HDIR/.history; fi
}

export -f pcmd

if [ -n "$ZSH_VERSION" ]; then
    source ~/shconfig/zsh_config.sh
elif [ -n "$BASH_VERSION" ]; then
    source ~/shconfig/bash_config.sh
fi
