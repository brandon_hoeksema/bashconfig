export HISTSIZE=-1
export HISTFILESIZE=-1
export HISTCONTROL=erasedups:ignorespace
export HISTIGNORE="ranger:ls:pwd:cd *"

PROMPT_COMMAND="history -n; history -w; history -c; history -r; pcmd $HOME/.bash_history"
